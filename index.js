const express = require('express'),
  app = express(),
  passport = require('passport'),
  bodyParser = require('body-parser'),
  session = require('express-session'),
  cookieParser = require('cookie-parser'),
  routes = require('./routes'),
  userRoutes = require('./routes/user'),
  quizRoutes = require('./routes/quiz'),
  path = require('path'),
  fs = require('fs'),
  mongoose = require('mongoose'),
  dbConfig = require('./config/dbConfig.js'),
  flash = require('connect-flash'),
  logger = require('morgan'),
  middlewarez = require('./middlewares'),
  http = require('http').Server(app),
  io = require('socket.io')(http);

io.on('connection', function (socket) {
  console.log('a user connected');
  socket.on('disconnect', function () {
    console.log('user disconnected');
  });
});
// Add headers
app.use(middlewarez.corsHeaders);
// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.static(path.join(__dirname, 'public')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session(
  {
    secret: 'ssshhhhh',
    cookie: {
      secure: false
    },
    resave: true,
    saveUninitialized: true
  }
));
app.use(flash());

// passort config
app.use(passport.initialize());
app.use(passport.session());
require('./config/localPassportStrategy'); // contains the passport google strategy


app.use("/", routes);

app.use('/user', userRoutes);
app.use('/quiz', quizRoutes);

//database connection
mongoose.connect(dbConfig.connectionString + dbConfig.dbName, { useNewUrlParser: true }, dbConnected);
function dbConnected(err) {
  if (err) throw err;
  http.listen(app.get('port'));
  console.log('Express server listening on port ' + app.get('port'));

}
