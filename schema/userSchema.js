//make our user collection accessible to this router.
var mongoose = require('mongoose'),
    bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;
var UserSchema = mongoose.Schema({
  firstname: { type: String, required: true},
  lastname: { type: String },
  username: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true }
}, { timestamps: true });

//add methods to this schema.

UserSchema.pre('save', function(next) {
    var user = this;
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();
    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);
        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);
            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

UserSchema.methods.checkPassword = function( pwd, cb) {
  bcrypt.compare(pwd, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};
UserSchema.virtual('fullName').
  get(function() { return this.firstname + ' ' + this.lastname; }).
  set(function(v) {
    this.firstname = v.substr(0, v.indexOf(' '));
    this.lastname = v.substr(v.indexOf(' ') + 1);
  });


var User = mongoose.model("User", UserSchema);

module.exports = User
