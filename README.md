# NTEG
Make sure mongod is running in the background.
postman API collection link -
https://www.getpostman.com/collections/344413bb01e400e21890
<b>How to start backend server with mongo?</b>
<ol>
	<li> Using Docker compose
		<ul>
			<li>To create respective images: docker-compose build</li>
			<li>To start containers: docker-compose up -d</li>
			<li>To see logs: docker-compose logs -f</li>
		</ul>
	</li>
	<li>Using Kubernetes (built inside Docker)
	(Only after images are built using docker-compose build)
		<ul>
			<li>Deployment(replicaset) and service for mongodb: kubectl create -f kube-mongo.yaml <li>
			<li>Deployment(replicaset) and service for backend: kubectl create -f kube-backend.yaml </li>
			<li>Find pods: kubectl get pods</li>
			<li>Logs: kubectl logs pod_name</li>
			<li>Terminate Services: kubectl delete svc mongo quiz-backend-servic </li>
			<li>Terminate deployments: kubectl delete deployment quiz-db-deployment quiz-backend-deployment </li>
		</ul>
	</li>
</ol>
If run individually or using docker-compose build and up, it runs on 3000 port.
If run using Kubernetes, it runs on 30000 port.
