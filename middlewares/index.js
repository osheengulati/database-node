const axios = require('axios');
const apis = require('../config/quizAPI');
const config = require('../config/config.js');
function corsHeaders(req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', config.FRONTEND_PROJECT_URL);
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);
  // Pass to next layer of middleware
  next();
}
function ensureAuthenticated(req, res, next) {
  if (req.user) {
    next();
  } else {
    res.status(401).json({ "message": "Not Logged in!" });
  }
}

const fetchQuizToken = async () => {
  console.log("requesting token from :" + apis.getToken);
  return performAxiosRequest(apis.getToken);
}
const performAxiosRequest = async (url) => {
  try {
    return axios.get(url)
  } catch (error) {
    console.error(error)
  }
}
module.exports = {
  corsHeaders: corsHeaders,
  ensureAuthenticated: ensureAuthenticated,
  performAxiosRequest: performAxiosRequest,
  fetchQuizToken: fetchQuizToken
}
