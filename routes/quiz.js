const axios = require('axios');
var express = require('express');
var router = express.Router();
var middlewarez = require('../middlewares');
const apis = require('../config/quizAPI');

//generates token
var token;
middlewarez.fetchQuizToken()
	.then(response => {
		token = response.data.token;
		console.log("token received :" + token);
	})
	.catch(err=>console.log(err));

router.get('/categories', function (req, res) {
	middlewarez.performAxiosRequest(apis.getCategories)
		.then(response => {
			res.json({ "categories": response.data.trivia_categories });
		});
});
router.get('/questions', middlewarez.corsHeaders, function (req, res) {
	var route = apis.getQuestions + token;
	if (req.body) {
		const {
			amount = 10,
			//can be multiple or boolean
			type = null,
			//can be a number mapped to category
			category = null,
			//easy, medium, hard
			difficulty = 'easy'
		} = req.query; //initialize variables from query string.
		route += amount ? `&amount=${amount}` : "";
		route += type ? `&type=${type}` : "";
		route += category ? `&category=${category}` : "";
		route += difficulty ? `&difficulty=${difficulty}` : "";

	}
	console.log("Requesting questions from url :" + route);
	middlewarez.performAxiosRequest(route)
		.then(response => {
			if (response.data.response_code = "0") {
				res.json({ "quizList": response.data.results });
			}
			else {
				res.status(400).json({
					"message": "invalid request",
					"response_code": response.data.response_code
				})
			}
		});

});
module.exports = router;
