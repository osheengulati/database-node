var express = require('express');
var router = express.Router();
var dbConfig = require('../config/dbConfig');
var Person = require('../schema/userSchema');
var passport = require('passport');


router.get('/', function(req, res) {
  if(req.user){
    res.json({"username": req.user.username});
  }
  else{
    res.status(401).json({message: "Not logged in"});
  }
});
router.post("/login",
    passport.authenticate("local", {failureFlash: true}),
    function (req, res, info) {
	if(req.user){
		console.log("Login - user = %j", req.user);
        	res.json(true);
	}
	else{
		res.json(false);
	}
});

router.post("/signup", function(req, res){
	console.log("body %j:", req.body);
  var personInfo = req.body; //Get the parsed information
      var newPerson = new Person(personInfo);
      newPerson.save(function(err, Person) {
        if (err) {
					if(err.code == 11000){
						console.error("Duplicate username error for ", personInfo.username);
					}
          res.status(500).json(err);
        } else {
          req.flash('info', 'Signup successful. Please login to continue');
          res.json(true);
        }
      });

});



module.exports = router;
