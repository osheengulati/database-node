var passport      = require('passport'),
  LocalStrategy   = require('passport-local').Strategy,
  Person    = require('../schema/userSchema');

//verify callback
var usernameAndPasswordStrategy =  function(username, password, done) {
    Person.findOne({ username: username }, function(err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      user.checkPassword(password, function(err, isMatch){
        if(err){
          return done(err);
        }
        if(isMatch){
          console.log('Authentication successful!');
          return done(null, user);
        }
        else{
          return done(null, false, {message: 'Wrong password!'});
        }
      });
    });
  }

//serialize function
function serializeUser(user, done){
  console.log('serializing: saving id= %s for username=%s', user._id, user.username);
  done(null, user._id);
}
//deserialize function
function deserializeUser(id, done) {
  console.log('Deserializing: converting id to user: %s ',id)
  Person.findOne({_id: id}, function(err, user) {
     done(err, user);
  });
}

passport.use(new LocalStrategy(usernameAndPasswordStrategy));
passport.serializeUser(serializeUser);
passport.deserializeUser(deserializeUser);
