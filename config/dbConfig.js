//replace mongo with localhost to connect local db
const connectionString = 'mongodb://mongo:27017/';
const dbName = "nteg";
const userCollectionName = "users";

module.exports = {
  connectionString : connectionString,
  dbName : dbName,
  userCollection : userCollectionName
}
